(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "./../mylib/mylib.sats"

(* ****** ****** *)

typedef vnam = string
typedef opnm = string

(* ****** ****** *)

datatype term = 
//
  | TMint of int
//
  | TMvar of vnam
  | TMlam of (vnam, term)
  | TMapp of (term, term)
  | TMifz of (term, term, term)
  | TMfix of (vnam, vnam, term) // Y(lam f.lam x.<body>)
  | TMopr of (opnm, termlst)
  | TMtup of (termlst) // tuple construction
  | TMprj of (term, int) // tuple projection

where termlst = list0(term)

(* ****** ****** *)

(*
fun
term_size
(tm0: term): int =
(
case+ tm0 of
| TMint _ => 1
| TMvar _ => 1
| TMlam (_, tm1) =>
  1 + term_size(tm1)
| TMapp (tm1, tm2) =>
  1 + term_size(tm1) + term_size(tm2)
| TMifz (tm1, tm2, tm3) =>
  1 + term_size(tm1) + term_size(tm2) + term_size(tm3)
| TMfix (_, _, tm1) => 1 + term_size(tm1)
)
*)

(* ****** ****** *)

extern
fun
print_term(term): void // stdout
and
prerr_term(term): void // stderr
and
fprint_term(FILEref, term): void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(tm) =
fprint_term(stdout_ref, tm)
implement
prerr_term(tm) =
fprint_term(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<term> = fprint_term

(* ****** ****** *)

implement
fprint_term(out, tm0) =
(
case+ tm0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tm1) =>
  fprint!(out, "TMlam(", x, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, x, tm1) =>
  fprint!(out, "TMfix(", f, "; ", x, "; ", tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
| TMtup(tms) =>
  fprint!(out, "TMtup(", tms, ")")
| TMprj(tm1, idx) =>
  fprint!(out, "TMprj(", tm1, "; ", idx, ")")
)

(* ****** ****** *)

val K =
let
  val x = TMvar("x")
in
  TMlam("x", TMlam("y", x))
end

(* ****** ****** *)

val
omega =
(
TMlam("x", TMapp(x, x))
) where
{
  val x = TMvar("x")
}

val
Omega = TMapp(omega, omega)

(* ****** ****** *)
//
fun
fomega(f: term): term =
let
  val x = TMvar("x")
in
TMlam
("x", TMapp(f, TMapp(x, x)))
end
//
fun
fOmega(f: term): term =
(
  TMapp(fomega(f), fomega(f))
)
//
(* ****** ****** *)

val () = println!("K = ", K)
val () = println!("omega = ", omega)
val () = println!("Omega = ", Omega)

(* ****** ****** *)

extern
fun
subst0
(tm0: term, x0: vnam, sub: term): term

implement
subst0
(tm0, x0, sub) =
let
fun
helper(tm: term): term = subst0(tm, x0, sub)
in
case+ tm0 of
| TMint _ => tm0
| TMvar(x1) =>
  if
  x0 = x1 then sub else tm0
| TMlam(x1, tm1) =>
  if
  x0 = x1
  then tm0
  else TMlam(x1, helper(tm1))
| TMapp(tm1, tm2) =>
  TMapp(helper(tm1), helper(tm2))
| TMifz(tm1, tm2, tm3) =>
  TMifz(helper(tm1), helper(tm2), helper(tm3))
| TMfix(f0, x1, tm1) =>
  if
  x0 = f0
  then tm0
  else
  (
  if
  x0 = x1
  then tm0
  else TMfix(f0, x1, helper(tm1))
  )
| TMopr(opr, tms) =>
  TMopr(opr, list0_map(tms, lam(tm) => helper(tm)))
| TMtup(tms) =>
  TMtup(list0_map(tms, lam(tm) => helper(tm)))
| TMprj(tm1, idx) =>
  TMprj(helper(tm1), idx)
end

(* ****** ****** *)

(*
extern
fun
interp1 : term -> term // call-by-name
*)
extern
fun
interp2 : term -> term // call-by-value
and
interp2_opr : term -> term // call-by-value

(* ****** ****** *)

(*
implement
interp1(tm0) =
(
case+ tm0 of
| TMint _ => tm0
| TMvar _ => tm0
| TMlam(x0, tm1) => tm0
| TMapp(tm1, tm2) =>
  let
    val tm1 = interp1(tm1)
  in
    case+ tm1 of
    | TMlam(x1, tm11) =>
      interp1(subst0(tm11, x1, tm2))
    | _(* non-TMlam *) => tm0
  end
| TMifz(tm1, tm2, tm3) =>
  let
  val tm1 = interp1(tm1)
  in
    case- tm1 of
    | TMint(i) =>
      if i = 0 then interp1(tm2) else interp2(tm3)
  end
| TMfix(f0, x1, tm1) =>
  TMlam(x1, subst0(tm1, f0, tm0))
)
*)

(* ****** ****** *)

implement
interp2(tm0) =
(
case+ tm0 of
| TMint _ => tm0
| TMvar _ => tm0
| TMlam(x0, tm1) => tm0
| TMapp(tm1, tm2) =>
  let
    val tm1 = interp2(tm1)
    val tm2 = interp2(tm2)
  in
    case+ tm1 of
    | TMlam(x1, tm11) =>
      interp2(subst0(tm11, x1, tm2))
    | _(* non-TMlam *) => tm0
  end
| TMifz(tm1, tm2, tm3) =>
  let
  val tm1 = interp2(tm1)
  in
    case- tm1 of
    | TMint(i) =>
      if i = 0 then interp2(tm2) else interp2(tm3)
  end
| TMfix(f0, x1, tm1) =>
  TMlam(x1, subst0(tm1, f0, tm0))
| TMopr(opr, tms) => interp2_opr(tm0)
| TMtup(tms) =>
  TMtup(list0_map(tms, lam(tm) => interp2(tm)))
| TMprj(tm1, idx) =>
  let
    val tm1 = interp2(tm1)
    val-TMtup(tms) = tm1 in list0_get_at_exn(tms, idx)
  end
)

(* ****** ****** *)


implement
interp2_opr(tm0) =
let
val-
TMopr(opr, tms) = tm0
//
val tms =
list0_map<term><term>(tms, lam(tm) => interp2(tm))
//
in
  case+ opr of
  | "+" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 + i2)
    end
  | "-" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 - i2)
    end
  | "*" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 * i2)
    end
  | "/" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 / i2)
    end
  | ">" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      if i1 > i2 then TMint(1) else TMint(0)
    end
  | "<" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      if i1 < i2 then TMint(1) else TMint(0)
    end
  | _ => let
      val () =
      prerrln!("interp2: TMopr: opr = ", opr) 
    in
      assertloc(false); exit(1)
    end
end
(* ****** ****** *)

(*
val _ = interp1(TMvar("x"))
*)
val _ = interp2(TMvar("x"))

(* ****** ****** *)

#define nil list0_nil
#define :: list0_cons
#define cons list0_cons

val TMfact =
(
TMfix("f", "n", TMifz(n, TMint(1), TMmul(n, TMapp(f, TMsub(n, TMint(1))))))
) where
{
//
  val f = TMvar("f")
  val n = TMvar("n")
//
  fun TMsub(x: term, y: term): term = TMopr("-", x :: y :: nil())
  fun TMmul(x: term, y: term): term = TMopr("*", x :: y :: nil())
//
}

(* ****** ****** *)

val TMfact2 =
(
TMfix(
  "f", "nr"
, TMifz(n, r, TMapp(f, TMtup(TMsub(n, TMint(1)):: TMmul(n, r) :: nil())))
)
) where
{
  val f = TMvar("f")
  val nr = TMvar("nr")
  val n = TMprj(nr, 0)
  val r = TMprj(nr, 1)
  fun TMsub(x: term, y: term): term = TMopr("-", x :: y :: nil())
  fun TMmul(x: term, y: term): term = TMopr("*", x :: y :: nil())
}

(* ****** ****** *)

implement main0() =
{
//
val () = println!("Hello from [main0]!")
//
val () =
println!
("fact(10) = ", interp2(TMapp(TMfact, TMint(10))))
val () =
println!
("fact2(10,1) = ", interp2(TMapp(TMfact2, TMtup(TMint(10)::TMint(1)::nil()))))
//
}

(* ****** ****** *)

(* end of [lambda0.dats] *)
