(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
#include
"share/HATS/atslib_staload_libats_libc.hats"
//
(* ****** ****** *)

#staload "./../mylib.sats"
#staload "./../mylib.dats"

(* ****** ****** *)
//
#define nil mylist_nil
//
#define :: mylist_cons
#define cons mylist_cons
//
(* ****** ****** *)

implement
main0() =
let
//
val
sqrt2 =
$extfcall(double, "sqrt", 2.0)
val () =
println!
("square root of 2 equals ", sqrt2)
//
val xs = 1 :: 2 :: 3 :: 4 :: 5 :: nil()
//
val () =
println!("|", xs, "| = ", mylist_length<int>(xs))
in
  // nothing  
end

(* ****** ****** *)

(* end of [test00.dats] *)
