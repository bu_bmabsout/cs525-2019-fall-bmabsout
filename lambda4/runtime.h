/*
// A basic runtime for lambda
*/

/* ****** ****** */

#include <stdio.h>
#include <stdlib.h>

/* ****** ****** */

extern
void*
mymalloc(size_t);

/* ****** ****** */

#define TAGint 1
#define TAGstr 2
#define TAGclo 3 // closure function

/* ****** ****** */

typedef
struct{ int tag; } lamval_;

typedef lamval_ *lamval;

/* ****** ****** */

typedef
struct{ int tag; int data; } lamval_int_;
typedef
struct{ int tag; char *data; } lamval_str_;

/* ****** ****** */

lamval
LAMVALint(int i)
{
  lamval_int p0;
  p0 = mymalloc(sizeof(lamval_int_));
  p0->tag = TAGint; p0->data = i; return (lamval)p0;
}

/* ****** ****** */

lamval
LAMVAL_tag(lamval x)
{
  return x->tag;
}

/* ****** ****** */

lamval
LAMVAL_add(lamval x, lamval y)
{
  /*
  assert(x->tag == TAGint);
  assert(y->tag == TAGint);
  */
  return
  LAMVALint(((lamval_int)x)->data + ((lamval_int)y)->data);
}

/* ****** ****** */

lamval
LAMVAL_sub(lamval x, lamval y)
{
  /*
  assert(x->tag == TAGint);
  assert(y->tag == TAGint);
  */
  return
  LAMVALint(((lamval_int)x)->data - ((lamval_int)y)->data);
}

/* ****** ****** */

lamval
LAMVAL_print(lamval x)
{
  /*
  assert(x->tag == TAGint);
  assert(y->tag == TAGint);
  */
  switch(x->tag)
    {
    case TAGint: printf("%i", (lamval_int)x->data); break;
    case TAGstr: printf("%s", (lamval_str)x->data); break
    case TAGclo: printf("<LAMVAL_CLO>"); break;
    default: printf("What???");
    }
}

/* ****** ****** */

lambda
fact(lamval n)
{

  lamval r0, r1, r2, r3;

  LAMVAL_ifz(n)
  {
    r0 = LAMVALint(1);
  }
  {
    r3 = LAMVALopr_sub(n, LAMVALint(1));
    r2 = fact(r3);
    r0 = LAMVALopr_mul(n, r2);
  }
  return r0;
  
    
  /*
  LAMVAL_ifz
    (n,
     LAMVALint(1);
     LAMVALopr_mul(r1, r2) where
     {
       r1 = n;
       r2 =
       fact(r3) where
	 {
	   r3 = LAMVALopr_sub(n, 1)
	 }
     }
  */
}
