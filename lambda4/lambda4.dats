(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload
UN =
"prelude/SATS/unsafe.sats"

(* ****** ****** *)

#staload "./../mylib/mylib.sats"

(* ****** ****** *)

abstype
tvar_type = ptr
typedef
tvar = tvar_type

(* ****** ****** *)

typedef tnam = string
typedef vnam = string
typedef opnm = string

(* ****** ****** *)
//
datatype type =
//
  | TPbas of tnam
//
  | TPext of tvar
//
  | TPfun of
    (type(*arg*), type(*res*))
  | TPtup of
    (type(*fst*), type(*snd*))
//
typedef typeopt = Option(type)
//
(* ****** ****** *)

datatype expr = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of
    (vnam, typeopt, expr)
  | TMapp of (expr, expr)
//
  | TMifz of (expr, expr, expr)
//
  | TMfix of
    ( vnam, vnam
    , typeopt(*arg*)
    , typeopt(*res*), expr) // Y(lam f.lam x.<body>)
//
  | TMopr of (opnm, exprlst)
//
  | TMtup of (expr, expr)
  | TMfst of expr | TMsnd of expr
//
  | TMlet of (vnam, expr, expr) // let x = t1 in t2 end
//
  | TManno of (expr, type)
//
where exprlst = list0(expr)

(* ****** ****** *)
//
abstype t3env // compilation environment
//
(* ****** ****** *)

datatype t3reg =
| T3REG of stamp
where stamp = int

(* ****** ****** *)

extern
fun t3reg_new(): t3reg

(* ****** ****** *)

local

val
theStamp = ref<int>(0)

in

implement
t3reg_new() =
let
val n = theStamp[] in theStamp[] := n+1; T3REG(n)
end

end

(* ****** ****** *)

datatype
t3val =
| T3Vint of int
| T3Vstr of string
| T3Vfun of string
| T3Vtmp of t3reg
| T3Varg of string
| T3Venv of (string, int)
| T3Vclo of (string, t3vals(*env*), t3inss(*body*), t3val(*res*))

and
t3ins =
| T3Imov of (t3reg, t3val)
| T3Itup of (t3reg, t3val, t3val)
| T3Ical of (t3reg, t3val, t3val)
| T3Iifz of (t3val, t3inss, t3inss)
| T3Iopr of (t3reg, opnm, list0(t3val))

where
t3vals = list0(t3val)
and
t3inss = list0(t3ins)

(* ****** ****** *)
//
extern
fun
inss_extend
(inss: t3inss, ins1: t3ins): t3inss
extern
fun
inss_append
(inss1: t3inss, inss2: t3inss): t3inss
//
overload extend with inss_extend of 1
overload append with inss_append of 1
//
(* ****** ****** *)

extern
fun
compile :
(t3env, expr) -> (t3inss, t3val)

extern
fun
compile_var :
(t3env, expr) -> t3val
and
compile_lam :
(t3env, expr) -> t3val

(* ****** ****** *)

implement
compile(env, e0) =
(
case- e0 of
| TMint(i0) =>
  (nil0(), T3Vint(i0))
| TMstr(s0) =>
  (nil0(), T3Vstr(s0))
//
| TMvar _ =>
  (nil0(), compile_var(env, e0))
//
| TMtup(e1, e2) =>
  let
  val res = t3reg_new()
  val
  (xss1, v1) = compile(env, e1)
  val
  (xss2, v2) = compile(env, e2)
  val ins0 = T3Itup(res, v1, v2)
  in
    (extend(append(xss1, xss2), ins0), T3Vtmp(res))
  end
| TMifz(e1, e2, e3) =>
  let
  val res = t3reg_new()
  val
  (xss1, v1) = compile(env, e1)
  val
  (xss2, v2) = compile(env, e2)
  val
  (xss3, v3) = compile(env, e3)
  val ins0 =
  T3Iifz
    ( v1
    , extend(xss2, T3Imov(res, v2))
    , extend(xss3, T3Imov(res, v3)))
  // T3Iifz
  in
    (extend(xss1, ins0), T3Vtmp(res))
  end
| TMlam _ => (nil0(), compile_lam(env, e0))
)

(* ****** ****** *)

extern
fun
t3fun_new(): string
extern
fun
fenv_make(env: t3env): t3vals

(* ****** ****** *)

extern
fun
t3var_is_arg(t3env, tnam): bool
extern
fun
t3var_is_env(t3env, tnam): bool
extern
fun
t3var_get_idx(t3env, tnam): int
extern
fun
t3var_is_let(t3env, tnam): bool
extern
fun
t3var_get_val(t3env, tnam): t3val

(* ****** ****** *)

implement
compile_var
(env, e0) = let
//
val-
TMvar(x0) = e0
//
in
//
ifcase
| t3var_is_arg(env, x0) => T3Varg(x0)
| t3var_is_let(env, x0) => t3var_get_val(env, x0)
| t3var_is_env(env, x0) => T3Venv(x0, t3var_get_idx(env, x0))
| _ (* else *) => let val () = assertloc(false) in T3Vint(0) end
//
end // end of [compile_var]

(* ****** ****** *)

implement
compile_lam
(env, e0) = let
//
val-
TMlam(x0, _, e1) = e0
//
val fnm = t3fun_new()
val fenv = fenv_make(env)
val env2 = compile_lam_env(env, x0)
val body = compile(env2, e1)
//
in
  T3Vclo(fnm, fenv, body.0, body.1)
end // end of [compile_lam]

(* ****** ****** *)

(* end of [lambda4.dats] *)
