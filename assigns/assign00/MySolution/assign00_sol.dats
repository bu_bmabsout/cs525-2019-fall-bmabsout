(* ****** ****** *)
//
// How to test
// ./assign00_sol_dats
//
// How to compile:
// myatscc assign00_sol.dats
//
(* ****** ****** *)
%{^
#include <math.h>
%}


#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
#include
"share/atspre_define.hats" // defines some names
#include
"share/HATS/atslib_staload_libats_libc.hats" // for libc
#staload _ =  "libats/libc/DATS/math.dats"


(* ****** ****** *)

#include "./../assign00.dats"
#include "./../Draw_a_sphere.dats"
(* ****** ****** *)

implement
factorial(n) =
if n > 0
then n * factorial(n-1) else 1

implement
int_test() =
go(0,1)
	where {
		fun go(n : int, i: int) =
			if i < 0
			then n+1
			else go(n + 1, i+i)
	}


implement
gheep(n: int) =
go(n, 2, 1, 2)
	where {
		fun go(n : int,i : int, f_2 : int, f_1 : int) =
			if n < 2
			then n + 1
			else if i = n
			then i * f_1 * f_2
			else go(n, i + 1, f_1, i * f_1 * f_2)
	}


(* ****** ****** *)
implement
intlist_append(il1, il2) =
go(reverse(il1,nil()), il2)
where {
    fun reverse(il : intlist, acc: intlist) =
        case+ il of
        | nil() => acc
        | h :: t => reverse(t, h :: acc)

    fun go(reversed: intlist, result: intlist): intlist =
        case+ reversed of
        | nil() => result
        | h :: t => go(t,h::result)
}


fun intlist_print(xs0: intlist): void =
(
case xs0 of
| intlist_nil() => println!()
| intlist_cons(x0, xs0) => (print!(x0, " ");  intlist_print(xs0))
)

//implement
//DrawSphere
//(
//  R: double, k: double, ambient: double
//) = let
//    fun normalize(v0: double, v1: double, v2: double): (double, double, double) = let
//        val len = sqrt(v0*v0+v1*v1+v2*v2)
//    in
//        (v0/len, v1/len, v2/len)
//    end // end of [normaPlize]
 
//    fun dot(v0: double, v1: double, v2: double, x0: double, x1: double, x2: double): double = let
//        val d = v0*x0+v1*x1+v2*x2
//        val sgn = gcompare_val_val<double> (d, 0.0)
//    in
//        if sgn < 0 then ~d else 0.0
//    end // end of [dot]
 
//    fun print_char(i: int): void =
//        if i = 0 then print!(".") else
//        if i = 1 then print!(":") else
//        if i = 2 then print!("!") else
//        if i = 3 then print!("*") else
//        if i = 4 then print!("o") else
//        if i = 5 then print!("e") else
//        if i = 6 then print!("&") else
//        if i = 7 then print!("#") else
//        if i = 8 then print!("%") else
//        if i = 9 then print!("@") else print!(" ")
 
//    val i_start = floor(~R)
//    val i_end = ceil(R)
//    val j_start = floor(~2 * R)
//    val j_end = ceil(2 * R)
//    val (l0, l1, l2) = normalize(30.0, 30.0, ~50.0)
 
//    fun loopj(j: int, j_end: int, x: double): void = let
//        val y = j / 2.0 + 0.5;
//        val sgn = gcompare_val_val<double> (x*x + y*y, R*R)
//        val (v0, v1, v2) = normalize(x, y, sqrt(R*R - x*x - y*y))
//        val b = pow(dot(l0, l1, l2, v0, v1, v2), k) + ambient
//        val intensity = 9.0 - 9.0*b
//        val sgn2 = gcompare_val_val<double> (intensity, 0.0)
//        val sgn3 = gcompare_val_val<double> (intensity, 9.0)
//    in
//    (   if sgn > 0 then print_char(10) else 
//        if sgn2 < 0 then print_char(0) else
//        if sgn3 >= 0 then print_char(8) else
//        print_char(g0float2int(intensity));
//        if j < j_end then loopj(j+1, j_end, x)
//    )
//    end // end of [loopj]
 
//    fun loopi(i: int, i_end: int, j: int, j_end: int): void = let
//        val x = i + 0.5
//        val () = loopj(j, j_end, x)
//        val () = println!()
//    in
//        if i < i_end then loopi(i+1, i_end, j, j_end)
//    end // end of [loopi]
 
//in
//    loopi(g0float2int(i_start), g0float2int(i_end), g0float2int(j_start), g0float2int(j_end))
//end
 
(* ****** ****** *)

//fun Maximum_triangle_path_sum_f(triangle : list(list(int))) = nil()

//implement
//Maximum_triangle_path_sum() = Maximum_triangle_path_sum_f(1::2::nil()/\pyramid_nil())

//datatype
//pyramid =
// | pyramid_nil of ()
// | pyramid_cons of (intlist, pyramid)
//
//#define /\ intlist_cons

implement
main0() = ()
where
{
	val () = println! ("int_test() = ",int_test())
	val () = println! ("ghaap(4)= ", ghaap(5), ", gheep(4)= ", gheep(5))
    val () = intlist_print(intlist_append(1::2::3::nil(), 4::5::6::nil()))
//	val () = DrawSphere(20.0, 4.0, .1)
} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign00_sol.dats] *)
