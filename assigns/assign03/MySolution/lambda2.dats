(* ****** ****** *)

#include
"share\
/atspre_staload.hats"
#include
"share\
/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "./../../../mylib/mylib.sats"
#staload "./../../../mylib/mylib.dats"

(* ****** ****** *)

typedef tnam = string
typedef vnam = string
typedef opnm = string

(* ****** ****** *)
//
datatype type =
  | TPbas of tnam
  | TPfun of
    (type(*arg*), type(*res*))
  | TPtup of
    (type(*fst*), type(*snd*))
//
(* ****** ****** *)

typedef
typelst = mylist(type)

(* ****** ****** *)

datatype ctype =
  | CTYPE of (typelst, type)

(* ****** ****** *)

typedef
ctypeopt = myoptn(ctype)

(* ****** ****** *)

extern
fun
opnm_get_ctype(x0: opnm): ctypeopt
extern
fun
opnm_set_ctype(x0: opnm, ct: ctype): void

(* ****** ****** *)

local

typedef
xcts =
mylist
($tup(opnm, ctype))
val
theCTmap =
ref<xcts>(mylist_nil())

in (*in-of-local*)

implement
opnm_get_ctype(x0) =
(
  loop(theCTmap[])
) where
{
fun
loop
(xcts: xcts): ctypeopt =
(
case+ xcts of
| mylist_nil() =>
  myoptn_none()
| mylist_cons(xct0, xcts) =>
  if
  x0 = xct0.0
  then myoptn_some(xct0.1) else loop(xcts)
)
} // opnm_get_ctype

implement
opnm_set_ctype(x0, ct) =
let
val xcts = theCTmap[]
in
  theCTmap[] := mylist_cons($tup(x0, ct), xcts)
end // opnm_set_ctype

end // end of [local]

(* ****** ****** *)
//
extern
fun
eq_type_type:
(type, type) -> bool
overload = with eq_type_type
//
(* ****** ****** *)

extern
fun
print_type(type): void // stdout
and
prerr_type(type): void // stderr
and
fprint_type(FILEref, type): void

overload print with print_type
overload prerr with prerr_type
overload fprint with fprint_type

(* ****** ****** *)

extern
fun
print_ctype(ctype): void // stdout
and
prerr_ctype(ctype): void // stderr
and
fprint_ctype(FILEref, ctype): void

overload print with print_ctype
overload prerr with prerr_ctype
overload fprint with fprint_ctype

(* ****** ****** *)

datatype expr = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of
    (vnam, type, expr)
  | TMapp of (expr, expr)
//
  | TMifz of
    (expr, expr, expr)
//
  | TMfix of
    (vnam, type, vnam, expr)
//
  | TMopr of (opnm, exprlst)
//
  | TMtup of (expr, expr)
  | TMfst of expr | TMsnd of expr
//
  | TMlet of (vnam, expr, expr) // let x = t1 in t2 end
//
where exprlst = mylist(expr)

(* ****** ****** *)

extern
fun
print_expr(expr): void // stdout
and
prerr_expr(expr): void // stderr
and
fprint_expr(FILEref, expr): void

overload print with print_expr
overload prerr with prerr_expr
overload fprint with fprint_expr

(* ****** ****** *)

implement
eq_type_type =
lam(tp1, tp2) =>
(
case+
(tp1, tp2) of
| (TPbas nm1,
   TPbas nm2) => (nm1 = nm2)
| (TPfun(tp11, tp12), 
   TPfun(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (TPtup(tp11, tp12), 
   TPtup(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (_, _) => false
)

(* ****** ****** *)
//
implement
fprint_val<type> = fprint_type
implement
fprint_val<expr> = fprint_expr
//
(* ****** ****** *)

implement
print_type(tp) =
fprint_type(stdout_ref, tp)
implement
prerr_type(tp) =
fprint_type(stderr_ref, tp)

(* ****** ****** *)

implement
fprint_type(out, tp0) =
(
case+ tp0 of
| TPbas(nm) =>
  fprint!(out, "TPbas(", nm, ")")
| TPfun(tp1, tp2) =>
  fprint!(out, "TPfun(", tp1, ", ", tp2, ")")
| TPtup(tp1, tp2) =>
  fprint!(out, "TPtup(", tp1, ", ", tp2, ")")
)

(* ****** ****** *)

implement
print_ctype(tp) =
fprint_ctype(stdout_ref, tp)
implement
prerr_ctype(tp) =
fprint_ctype(stderr_ref, tp)

(* ****** ****** *)

implement
fprint_ctype(out, ct0) =
(
case+ ct0 of
| CTYPE(tps, tp1) =>
  fprint!(out, "CTYPE(", tps, "; ", tp1, ")")
)

(* ****** ****** *)

implement
print_expr(tm) =
fprint_expr(stdout_ref, tm)
implement
prerr_expr(tm) =
fprint_expr(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<expr> = fprint_expr

(* ****** ****** *)

implement
fprint_expr(out, tm0) =
(
case+ tm0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMstr(x) =>
  fprint!(out, "TMstr(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tp0, tm1) =>
  fprint!
  ( out
  , "TMlam(", x, "; ", tp0, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, tp0, x, tm1) =>
  fprint!(out, "TMfix(", f, ": ", tp0 , "; ", x, "; ", tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
//
| TMfst(tm1) =>
  fprint!(out, "TMfst(", tm1, ")")
| TMsnd(tm1) =>
  fprint!(out, "TMsnd(", tm1, ")")
| TMtup(tm1, tm2) =>
  fprint!(out, "TMtup(", tm1, "; ", tm2, ")")
//
| TMlet(x0, tm1, tm2) =>
  fprint!(out, "TMlet(", x0, "; ", tm1, "; ", tm2, ")")
)

(* ****** ****** *)

exception IllTyped0 of ()

(* ****** ****** *)

typedef tctx =
mylist($tup(vnam, type))


(* ****** ****** *)
//
#define nil mylist_nil
//
#define :: mylist_cons
#define cons mylist_cons
(* ****** ****** *)

datatype maybe(a:t@ype) =
  | Nothing(a) of ()
  | Just(a) of a

fun type_lookup(x:vnam, l:tctx): maybe(type) =
  case+ l of
  | nil() => Nothing
  | x_t::rest =>
    if eq_string_string(x_t.0, x)
    then Just x_t.1
    else type_lookup(x, rest)

fun {a:t@ype} {b:t@ype} map
(
  l: mylist(a), f: cfun(a, b)
) : mylist(b) =
  (
  case+ l of
  | x::xs =>
      f x :: map<a><b>(xs, f)
  | nil() => nil
  )

(* ****** ****** *)

fun {a:t@ype} eq_list(l1:mylist(a), l2:mylist(a), equality:cfun2(a, a, bool)):bool =
  case+ (l1,l2) of
  | (nil(),nil()) => true
  | (nil(), _) => false
  | (_, nil()) => false
  | (x1::xs1, x2::xs2) => equality(x1, x2) && eq_list<a>(xs1,xs2, equality)

extern
fun oftype0(expr): type
extern
fun oftype1(tctx, expr): type

(* ****** ****** *)

val TPint = TPbas("int")
val TPstr = TPbas("string")
val TPbool = TPbas("bool")
val TPvoid = TPbas("void")

(* ****** ****** *)

implement
oftype0(tm0) =
oftype1(mylist_nil(), tm0)

implement
oftype1(tctx0, tm0) =
(
case- tm0 of
//
  | TMint _ => TPint
  | TMstr _ => TPstr
  | TMvar v =>
    (case+ type_lookup(v, tctx0) of
     | Just t => t
     | Nothing => $raise IllTyped0())

  | TMlam
    (x0, tp1, tm2) =>
    let
      val
      tctx1 =
      mylist_cons
      ($tup(x0, tp1), tctx0)
    in
      TPfun(tp1, oftype1(tctx1, tm2))
    end
//
  | TMapp(tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    in
      case- tp1 of
      | TPfun(tp11, tp12) =>
        if
        tp11 = tp2
        then tp12 else $raise IllTyped0()
    end
//
  | TMifz(tm1, tm2, tm3) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    val tp3 = oftype1(tctx0, tm3)
    in
      if
      tp1 = TPint
      then
      (
      if tp2 = tp3 then tp2 else $raise IllTyped0()
      )
      else $raise IllTyped0()
    end
    //
  | TMfix(x, ft, f, expr) =>
    let
    val- TPfun(xt,_) = ft
    val tctx1 = $tup(x,xt)::$tup(f,ft)::tctx0
    in
    if ft = TPfun(xt, oftype1(tctx1, expr))
    then ft
    else $raise IllTyped0()
    end
    //
  | TMopr(opr, exprlst) =>
    let
    val types = map(exprlst, lam x => oftype1(tctx0,x))
    in
      case+ opnm_get_ctype(opr) of
      | myoptn_none() => $raise IllTyped0()
      | myoptn_some(CTYPE(typelst, return_type)) =>
        if eq_list(types,typelst, lam (a,b) => a = b)
        then return_type
        else $raise IllTyped0()
    end
    //
  | TMtup(tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    in
      TPtup(tp1, tp2)
    end
//
  | TMfst(tm1) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    in
      case- tp1 of TPtup(tp11, _) => tp11
    end
  | TMsnd(tm1) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    in
      case- tp1 of TPtup(_, tp12) => tp12
    end
//
  | TMlet(x0, tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tctx1 =
    mylist_cons($tup(x0, tp1), tctx0)
    in
       oftype1(tctx1, tm2)
    end
)


//
(* ****** ****** *)

val () =
opnm_set_ctype
("+", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("-", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("*", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("/", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("<", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
(">", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("<=", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
(">=", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
(">=", CTYPE(TPint :: TPint :: nil(), TPint))

(* ****** ****** *)


//val TMint1 = TMint(1)
//val () =
//println!("oftype(TMint1) = ", oftype0(TMint1))
//val () =
//println!("oftype(TMapp...) = ", oftype0(TMapp(TMint1, TMint1)))


(* ****** ****** *)

implement main0() = 
let 
val () = println!("opr test:", oftype0(TMopr(">",TMint(1)::TMint(2)::nil())))
//val () = println!("opr failing:", oftype0(TMopr(">",TMstr("test")::TMint(2)::nil())))
val () = println!("tmfix test:", oftype0(TMfix("x", TPfun(TPint, TPint), "f", TMapp(TMvar("f"),TMint(3)))))
//val () = println!("tmfix failing:", oftype0(TMfix("x", TPfun(TPint, TPint), "f", TMapp(TMvar("f"),TMstr("test")))))

in () end
(* ****** ****** *)

(* end of [lambda2.dats] *)

