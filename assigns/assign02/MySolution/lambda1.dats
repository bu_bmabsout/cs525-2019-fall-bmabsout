(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "./../../../mylib/mylib.sats"

(* ****** ****** *)

typedef vnam = string
typedef opnm = string

(* ****** ****** *)

datatype term = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of (vnam, term)
  | TMapp of (term, term)
//
  | TMifz of (term, term, term)
//
  | TMfix of (vnam, vnam, term) // Y(lam f.lam x.<body>)
//
  | TMopr of (opnm, termlst)
//
  | TMtup of (termlst) // tuple construction
  | TMprj of (term, int) // tuple projection
//
  | TMlet of (vnam, term, term) // let x = t1 in t2 end
//
where termlst = list0(term)

(* ****** ****** *)

extern
fun
print_term(term): void // stdout
and
prerr_term(term): void // stderr
and
fprint_term(FILEref, term): void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(tm) =
fprint_term(stdout_ref, tm)
implement
prerr_term(tm) =
fprint_term(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<term> = fprint_term

(* ****** ****** *)

implement
fprint_term(out, tm0) =
(
case+ tm0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMstr(x) =>
  fprint!(out, "TMstr(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tm1) =>
  fprint!(out, "TMlam(", x, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, x, tm1) =>
  fprint!(out, "TMfix(", f, "; ", x, "; ", tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
| TMtup(tms) =>
  fprint!(out, "TMtup(", tms, ")")
| TMprj(tm1, idx) =>
  fprint!(out, "TMprj(", tm1, "; ", idx, ")")
| TMlet(x0, tm1, tm2) =>
  fprint!(out, "TMlet(", x0, "; ", tm1, "; ", tm2, ")")
)

(* ****** ****** *)

datatype
value =
| VALint of int
| VALstr of string
| VALtup of values
| VALlam of (term, envir)
| VALfix of (term, envir)

where
values = list0(value)
and
envir = list0($tup(vnam, value))

(* ****** ****** *)

extern
fun
print_value(value): void // stdout
and
prerr_value(value): void // stderr
and
fprint_value(FILEref, value): void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)

implement
print_value(x0) =
fprint_value(stdout_ref, x0)
implement
prerr_value(x0) =
fprint_value(stderr_ref, x0)

(* ****** ****** *)

implement
fprint_val<value> = fprint_value

(* ****** ****** *)

implement
fprint_value(out, vl0) =
(
case+ vl0 of
| VALint(x) =>
  fprint!(out, "VALint(", x, ")")
| VALstr(x) =>
  fprint!(out, "VALstr(", x, ")")
| VALtup(xs) =>
  fprint!(out, "VALtup(", xs, ")")
| VALlam(tm0, env) =>
  fprint!(out, "VALlam(", tm0, "; ", "...", ")")
| VALfix(tm0, env) =>
  fprint!(out, "VALfix(", tm0, "; ", "...", ")")
)

(* ****** ****** *)

extern
fun
interp0 : term -> value
extern
fun
interp1 : (term, envir) -> value

extern
fun
interp1_var : (term, envir) -> value
extern
fun
interp1_tup : (term, envir) -> value
extern
fun
interp1_app : (term, envir) -> value
extern
fun
interp1_opr : (opnm ,termlst, envir) -> value

(* ****** ****** *)

implement
interp0(t0) =
interp1(t0, list0_nil())

implement
interp1(t0, env) =
(
case+ t0 of
| TMint(i) => VALint(i)
| TMstr(s) => VALstr(s)
| TMvar(x) =>
  interp1_var(t0, env)
| TMtup(ts) =>
  interp1_tup(t0, env)
//
| TMlam(x, t) => VALlam(t0, env)
//
| TMapp(_, _) =>
  interp1_app(t0, env)
//
| TMifz(t1, t2, t3) =>
  let
    val v1 = interp1(t1, env)
  in
    case- v1 of
    | VALint(i1) =>
      if i1 = 0
      then interp1(t2, env) else interp1(t3, env)
  end
//
| TMfix(f, x, t) => VALfix(t0, env)
//
| TMlet(x0, t1, t2) =>
  let
    val v1 = interp1(t1, env)
  in
    interp1(t2, env1) where
    {
      val env1 =
      list0_cons($tup(x0, v1), env)
    }
  end
| TMopr(opr, tms) => interp1_opr(opr, tms, env)
| TMprj(tm1, idx) =>
  let
    val tm1 = interp1(tm1, env)
    val-VALtup(tms) = tm1 in list0_get_at_exn(tms, idx)
  end
)

(* ****** ****** *)

implement
interp1_opr(opr, tms, env) =
let
val tms =
list0_map<term><value>(tms, lam(tm) => interp1(tm, env))
//
in
  case+ opr of
  | "+" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-VALint(i1) = tm1 and VALint(i2) = tm2
    in
      VALint(i1 + i2)
    end
  | "-" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-VALint(i1) = tm1 and VALint(i2) = tm2
    in
      VALint(i1 - i2)
    end
  | "*" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-VALint(i1) = tm1 and VALint(i2) = tm2
    in
      VALint(i1 * i2)
    end
  | "/" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-VALint(i1) = tm1 and VALint(i2) = tm2
    in
      VALint(i1 / i2)
    end
  | ">" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-VALint(i1) = tm1 and VALint(i2) = tm2
    in
      if i1 > i2 then VALint(1) else VALint(0)
    end
  | "<" =>
    let
      val-list0_cons(tm1, tms) = tms
      val-list0_cons(tm2, tms) = tms
      val-VALint(i1) = tm1 and VALint(i2) = tm2
    in
      if i1 < i2 then VALint(1) else VALint(0)
    end
  | _ => let
      val () =
      prerrln!("interp1: TMopr: opr = ", opr) 
    in
      assertloc(false); exit(1)
    end
end

(* ****** ****** *)

implement
interp1_var
  (t0, env) =
let
val-TMvar(x0) = t0
in
(
  loop(env)
) where
{
fun
loop(xvs: envir): value =
(
case+ xvs of
| list0_nil() =>
  let
  val () =
  println!
  ("interp1_val: x0 = ", x0)
  in
  let
  val () = assertloc(false) in exit(1)
  end
  end

| list0_cons(xv, xvs) =>
  if x0 = xv.0 then xv.1 else loop(xvs)
)
}
end // end of [interp1_val]

(* ****** ****** *)

implement
interp1_tup
  (t0, env) =
let
val-TMtup(ts) = t0
in

VALtup
(list0_map(ts, lam(t) => interp1(t, env)))

end

(* ****** ****** *)

implement
interp1_app
  (t0, env) =
let
val-TMapp(t1, t2) = t0
//
val v1 = interp1(t1, env)
val v2 = interp1(t2, env)
//
in
//
case- v1 of
| VALlam(tf, env1) =>
  let
    val-TMlam(x, t) = tf
  in
    interp1(t, env2) where
    {
      val env2 =
      list0_cons($tup(x, v2), env1)
    }
  end
| VALfix(tf, env1) =>
  let
    val-TMfix(f, x, t) = tf
   in
     interp1(t, env2) where
     {
       val env2 =
       list0_cons( $tup(f, v1), env1 )
       val env2 =
       list0_cons( $tup(x, v2), env2 )
     }
   end
//
end

(* ****** ****** *)

typedef
int4 = (int, int, int, int)
//
val theCoins = (1, 5, 10, 25): int4
//
fun coin_get
  (n: int): int =
(
  if n = 0 then theCoins.0
  else if n = 1 then theCoins.1
  else if n = 2 then theCoins.2
  else if n = 3 then theCoins.3
  else ~1 (* erroneous value *)
) (* end of [coin_get] *)
//
fun coin_change
  (sum: int): int = let
  fun aux (sum: int, n: int): int =
    if sum > 0 then
     (if n >= 0 then aux (sum, n-1) + aux (sum-coin_get(n), n) else 0)
    else (if sum < 0 then 0 else 1)
  // end of [aux]
in
  aux (sum, 3)
end // end of [coin_change]

(* ****** ****** *)
#define :: list0_cons
#define nil list0_nil

val TM_theCoins = TMtup(TMint(1)::TMint(5)::TMint(10)::TMint(25)::nil)

val TM_coin_get =
TMlet("theCoins"
, TM_theCoins
, TMlam("n"
  , TMifz(TMopr(">",TMvar("n")::TMint(~1)::nil)
    , TMint(~1)
    , TMifz(TMopr(">",TMvar("n")::TMint(0)::nil)
      , TMprj(TMvar("theCoins"), 0)
      , TMifz(TMopr(">",TMvar("n")::TMint(1)::nil)
        , TMprj(TMvar("theCoins"), 1)
        , TMifz(TMopr(">",TMvar("n")::TMint(2)::nil)
          , TMprj(TMvar("theCoins"), 2)
          , TMifz(TMopr(">",TMvar("n")::TMint(3)::nil)
            , TMprj(TMvar("theCoins"), 3)
            , TMint(~1)
            )
          )
        )
      )
    )
  )
)

val TM_coin_change =
TMlet("coin_get"
, TM_coin_get
, TMlam("sum",
    TMlet("aux"
    , TMfix("f", "sum_n"
      , TMlet("sum", TMprj(TMvar("sum_n"), 0)
        , TMlet("n", TMprj(TMvar("sum_n"), 1)
          , TMifz(TMopr(">", TMvar("sum")::TMint(0)::nil)
            , TMifz(TMopr("<", TMvar("sum")::TMint(0)::nil)
              , TMint(1)
              , TMint(0)
              )
            , TMifz(TMopr("<", TMvar("n")::TMint(0)::nil)
              , TMopr("+"
                , TMapp(TMvar("f")
                  , TMtup(
                      TMvar("sum")
                    ::TMopr("-",TMvar("n")::TMint(1)::nil)
                    ::nil
                    )
                  )
                ::TMapp(
                    TMvar("f")
                  , TMtup(
                      TMopr("-"
                      , TMvar("sum")
                      ::TMapp(TMvar("coin_get"), TMvar("n"))
                      ::nil
                      )
                      ::TMvar("n")::nil
                    )
                  ) 
                ::nil
                )
              , TMint(0)
              )
            )
          )
        )
      )
    , TMapp(TMvar("aux"), TMtup(TMvar("sum")::TMint(3)::nil))
    )
  )
)
(* ****** ****** *)

implement main0() =
{
  val () =
    println!("coin_get:", coin_change(400))
  val () =
    println!("coin_get_lam:", interp0(TMapp(TM_coin_change, TMint(400))))

}

(* ****** ****** *)

(* end of [lambda1.dats] *)

