(* ****** ****** *)

#include
"share\
/atspre_staload.hats"
#include
"share\
/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload
UN =
"prelude/SATS/unsafe.sats"

(* ****** ****** *)

#staload "./../../../mylib/mylib.sats"
#staload "./../../../mylib/mylib.dats"

(* ****** ****** *)
//
#define nil mylist_nil
//
#define :: mylist_cons
#define cons mylist_cons
//
(* ****** ****** *)
//
#define none myoptn_none
#define some myoptn_some
//
(* ****** ****** *)

abstype
tvar_type = ptr
typedef
tvar = tvar_type

(* ****** ****** *)

typedef tnam = string
typedef vnam = string
typedef opnm = string

(* ****** ****** *)
//
datatype type =
//
  | TPbas of tnam
//
  | TPext of tvar
//
  | TPfun of
    (type(*arg*), type(*res*))
  | TPtup of
    (type(*fst*), type(*snd*))
//
(* ****** ****** *)

typedef typeopt = myoptn(type)

(* ****** ****** *)
//
extern
fun
tvar_new(): tvar
and
type_new(): type
//
extern
fun
eq_tvar_tvar
(tvar, tvar): bool
overload = with eq_tvar_tvar
//
extern
fun
tvar_get_type
(tvar): myoptn(type)
extern
fun
tvar_set_type
(X: tvar, sol: type): void
//
overload .type with tvar_get_type
overload .type with tvar_set_type
//
(* ****** ****** *)

local

absimpl
tvar_type =
ref(typeopt)

in

implement
tvar_new() =
ref<typeopt>(none)

implement
eq_tvar_tvar
  (X, Y) =
(
  $UN.cast{ptr}(X)
  =
  $UN.cast{ptr}(Y)
)
implement geq_val_val<tvar>(x, y) = $effmask_all(eq_tvar_tvar(x, y))

implement
tvar_get_type(X) = X[]
implement
tvar_set_type(X, T) = (X[] := some(T))

end // end of [local]

(* ****** ****** *)

implement
type_new() =
TPext(tvar_new())


extern
fun
print_type(type): void // stdout
and
prerr_type(type): void // stderr
and
fprint_type(FILEref, type): void

overload print with print_type
overload prerr with prerr_type
overload fprint with fprint_type
(* ****** ****** *)

fun
type_eval
(T: type): type =
(
case+ T of
| TPext(X) =>
  let
  val opt = X.type()
  in
  case+ opt of
  | none() => T
  | some(T) => type_eval(T)
  end
| _ (* non-TPext *) => T
)

overload eval with type_eval

(* ****** ****** *)

extern
fun
occurs(tvar, type): bool

implement occurs(X, t) =
  case+ t of
  | TPbas(tnam) => false
  | TPext(Y) =>
    if(X = Y)
    then true
    else 
      (
        case+ Y.type() of
        | none() => false
        | some(T) => occurs(X, T)
      )
  | TPfun(arg_type, res_type) =>
      occurs(X, arg_type) || occurs(X, res_type)
  | TPtup(fst_type, snd_type) =>
      occurs(X, fst_type) || occurs(X, snd_type)



extern
fun
unify:
(type, type) -> bool

implement
unify(T1, T2) =
let
val T1 = eval(T1)
val T2 = eval(T2)
//
fun
auxvar
(X1: tvar, T2: type): bool =
(
case+ T2 of
| TPext(X2) =>
  if X1 = X2
  then true
  else (X1.type(T2); true)
| _(*non-TPext*) =>
  if occurs(X1, T2)
  then false else (X1.type(T2); true)
)
//
in
case+
(T1, T2) of
|
(TPext(X1), _) => auxvar(X1, T2)
|
(_, TPext(X2)) => auxvar(X2, T1)
|
(TPbas(nm1), TPbas(nm2)) => nm1 = nm2
|
(TPfun(T11, T12), TPfun(T21, T22)) => 
 unify(T11, T21) && unify(T12, T22)
|
(TPtup(T11, T12), TPtup(T21, T22)) => 
 unify(T11, T21) && unify(T12, T22)
//
| (_, _) => false // unification failed
//
end

(* ****** ****** *)
(*
//
extern
fun
eq_type_type:
(type, type) -> bool
overload = with eq_type_type
//
*)
(* ****** ****** *)

(* ****** ****** *)

typedef typeopt = myoptn(type)

(* ****** ****** *)

datatype expr = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of
    (vnam, typeopt, expr)
  | TMapp of (expr, expr)
//
  | TMifz of (expr, expr, expr)
//
  | TMfix of
    ( vnam, vnam
    , typeopt(*arg*)
    , typeopt(*res*), expr) // Y(lam f.lam x.<body>)
//
  | TMopr of (opnm, exprlst)
//
  | TMtup of (expr, expr)
  | TMfst of expr | TMsnd of expr
//
  | TMlet of (vnam, expr, expr) // let x = t1 in t2 end
//
  | TManno of (expr, type)
//
where exprlst = mylist(expr)

(* ****** ****** *)

extern
fun
print_expr(expr): void // stdout
and
prerr_expr(expr): void // stderr
and
fprint_expr(FILEref, expr): void

overload print with print_expr
overload prerr with prerr_expr
overload fprint with fprint_expr

(* ****** ****** *)

(*
implement
eq_type_type =
lam(tp1, tp2) =>
(
case+
(tp1, tp2) of
| (TPbas nm1,
   TPbas nm2) => (nm1 = nm2)
| (TPfun(tp11, tp12), 
   TPfun(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (TPtup(tp11, tp12), 
   TPtup(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (_, _) => false
)
*)

(* ****** ****** *)

implement
print_type(tp) =
fprint_type(stdout_ref, tp)
implement
prerr_type(tp) =
fprint_type(stderr_ref, tp)

(* ****** ****** *)

implement
fprint_type(out, tp0) =
(
case+ tp0 of
| TPext(tv) =>
  let
  val () = fprint!(out, "TPext(");
  val () =
      (
        case+ tv.type() of
        | none() => fprint!(out, "...")
        | some(t) => fprint!(out, t)
      )
  val () = fprint!(")")
  in () end
| TPbas(nm) =>
  fprint!(out, "TPbas(", nm, ")")
| TPfun(tp1, tp2) =>
  fprint!(out, "TPfun(", tp1, ", ", tp2, ")")
| TPtup(tp1, tp2) =>
  fprint!(out, "TPtup(", tp1, ", ", tp2, ")")
)

(* ****** ****** *)

implement
print_expr(tm) =
fprint_expr(stdout_ref, tm)
implement
prerr_expr(tm) =
fprint_expr(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<type> = fprint_type
implement
fprint_val<expr> = fprint_expr

(* ****** ****** *)

implement
fprint_expr(out, tm0) =
(
case+ tm0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMstr(x) =>
  fprint!(out, "TMstr(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tp0, tm1) =>
  fprint!
  ( out
  , "TMlam(", x, "; ", tp0, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, x, tp1, tp2, tm1) =>
  fprint!(out, "TMfix(", f, "; ", x, "; ", tp1, "; ", tp2, tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
//
| TMfst(tm1) =>
  fprint!(out, "TMfst(", tm1, ")")
| TMsnd(tm1) =>
  fprint!(out, "TMsnd(", tm1, ")")
| TMtup(tm1, tm2) =>
  fprint!(out, "TMtup(", tm1, "; ", tm2, ")")
//
| TMlet(x0, tm1, tm2) =>
  fprint!(out, "TMlet(", x0, "; ", tm1, "; ", tm2, ")")
//
| TManno(tm1, tp2) =>
  fprint!(out, "TManno(", tm1, "; ", tp2, ")")
)

(* ****** ****** *)
(* ****** ****** *)


exception IllTyped0 of ()
exception IllTyped1 of (expr)
exception IllTyped2 of (expr, type)

(* ****** ****** *)

typedef tctx =
mylist($tup(vnam, type))

extern
fun oftype0(expr): type
extern
fun oftype1(tctx, expr): type
(* ****** ****** *)

fun {a:t@ype} {b:t@ype} map
(
  l: mylist(a), f: cfun(a, b)
) : mylist(b) =
  (
  case+ l of
  | x::xs =>
      f x :: map<a><b>(xs, f)
  | nil() => nil
  )

fun {a:t@ype} eq_list(l1:mylist(a), l2:mylist(a), equality:cfun2(a, a, bool)):bool =
  case+ (l1,l2) of
  | (nil(),nil()) => true
  | (nil(), _) => false
  | (_, nil()) => false
  | (x1::xs1, x2::xs2) => equality(x1, x2) && eq_list<a>(xs1,xs2, equality)

typedef
typelst = mylist(type)

(* ****** ****** *)

val TPint = TPbas("int")
val TPstr = TPbas("string")
val TPbool = TPbas("bool")
val TPvoid = TPbas("void")

(* ****** ****** *)

datatype maybe(a:t@ype) =
  | Nothing(a) of ()
  | Just(a) of a

datatype ctype =
  | CTYPE of (typelst, type)
  

fun opr_type(opr:opnm, exprs: exprlst, context:tctx): type =
  let
    val types = map(exprs, lam x => oftype1(context,x))
    fun opnm_get_ctype(l: mylist($tup(opnm,ctype)), opr: opnm):maybe(ctype) =
    (
      case+ l of
      | nil() => Nothing()
      | x::rest =>
        if x.0 = opr
        then Just(x.1)
        else opnm_get_ctype(rest, opr) 
    )

  in
    case+ opnm_get_ctype(opr_types, opr) of
    | Nothing() => $raise IllTyped0()
    | Just(CTYPE(typelst, return_type)) =>
      if eq_list(types,typelst, lam (a,b) => unify(a, b))
      then return_type
      else $raise IllTyped0()
  end
  where {
    val opr_types =
    $tup("+", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup("-", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup("*", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup("/", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup("<", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup(">", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup("<=", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup(">=", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup(">=", CTYPE(TPint :: TPint :: nil(), TPint))
    :: $tup("%", CTYPE(TPint :: TPint :: nil(), TPint))
    :: nil()
  }



(* ****** ****** *)

fun type_lookup(x:vnam, l:tctx): maybe(type) =
  case+ l of
  | nil() => Nothing
  | x_t::rest =>
    if eq_string_string(x_t.0, x)
    then Just x_t.1
    else type_lookup(x, rest)

implement
oftype0(tm0) =
oftype1(nil(), tm0)

implement
oftype1(tctx0, tm0) =
(
case+ tm0 of
//
| TMint _ => TPint
| TMstr _ => TPstr
//
| TMvar x =>
  (
  case+ type_lookup(x, tctx0) of
    | Nothing() => $raise IllTyped0() // variable not in scope
    | Just(x_type) => x_type
  )
//
| TMlam
  (x0, tp1, tm2) =>
  let
    val tp1 =
    (
    case+ tp1 of
    | none() =>
      type_new()
    | some(tp1) => tp1
    ) : type // end-of-val
    val
    tctx1 =
    $tup(x0, tp1)::tctx0
  in
    TPfun(tp1, oftype1(tctx1, tm2))
  end
//
| TMapp(tm1, tm2) =>
  let
  val tp1 = oftype1(tctx0, tm1)
  val tp2 = oftype1(tctx0, tm2)
  val new_type = type_new()
  in
    if unify(tp1, TPfun(tp2, new_type))
    then new_type
    else $raise IllTyped0()
  end
//
| TMifz(tm1, tm2, tm3) =>
  let
  val tp1 = oftype1(tctx0, tm1)
  val tp2 = oftype1(tctx0, tm2)
  val tp3 = oftype1(tctx0, tm3)
  in
    if
      unify(tp1, TPint) && unify(tp2, tp3)
    then
      tp2
    else $raise IllTyped0()
  end
//

| TMfix(f, x, opt_x_type, opt_res_type, body) =>
  let
    val x_type = 
      (
        case+ opt_x_type of
          | none() => type_new()
          | some(T) => T
      )
    val body_type =
      (
        case+ opt_res_type of
          | none() => type_new()
          | some(T) => T
      )
    val f_type = TPfun(x_type, body_type)
    val
      tctx1 = $tup(f, f_type)::$tup(x, x_type)::tctx0
  in
    if unify(oftype1(tctx1, body), body_type)
    then f_type
    else $raise IllTyped0()
  end
//
| TMtup(tm1, tm2) =>
  let
  val tp1 = oftype1(tctx0, tm1)
  val tp2 = oftype1(tctx0, tm2)
  in
    TPtup(tp1, tp2)
  end
//
| TMfst(tm1) =>
  let
  val tp1 = oftype1(tctx0, tm1)
  val fst_type = type_new()
  in
    if unify(tp1, TPtup(fst_type, type_new()))
    then
      fst_type
    else $raise IllTyped0()
  end
//
| TMsnd(tm1) =>
  let
  val tp1 = oftype1(tctx0, tm1)
  val snd_type = type_new()
  in
    if unify(tp1, TPtup(type_new(), snd_type))
    then
      snd_type
    else $raise IllTyped0()
  end
//
| TMlet(x, tm1, tm2) =>
  let
  val x_type = oftype1(tctx0, tm1)
  val tctx1 =
    $tup(x, x_type)::tctx0
  in
    oftype1(tctx1, tm2)
  end
//
| TManno (expr, expr_type) =>
  if(unify(oftype1(tctx0, expr), expr_type))
  then expr_type
  else $raise IllTyped0()
| TMopr (opr, expr_list) => opr_type(opr, expr_list, tctx0)

)






(* ****** ****** *)

val TM_theCoins = TMtup(TMint(1),TMint(5))

val TM_coin_get =
TMlet("theCoins"
, TM_theCoins
, TMlam("n", none()
  , TMifz(TMopr(">",TMvar("n")::TMint(~1)::nil)
    , TMint(~1)
    , TMifz(TMopr(">",TMvar("n")::TMint(0)::nil)
      , TMfst(TMvar("theCoins"))
      , TMifz(TMopr(">",TMvar("n")::TMint(1)::nil)
        , TMsnd(TMvar("theCoins"))
        , TMint(~1)
        )
      )
    )
  )
)

val TM_coin_change =
TMlet("coin_get"
, TM_coin_get
, TMlam("sum", none(),
    TMlet("aux"
    , TMfix("f", "sum_n", none(), none()
      , TMlet("sum", TMfst(TMvar("sum_n"))
        , TMlet("n", TMsnd(TMvar("sum_n"))
          , TMifz(TMopr(">", TMvar("sum")::TMint(0)::nil)
            , TMifz(TMopr("<", TMvar("sum")::TMint(0)::nil)
              , TMint(1)
              , TMint(0)
              )
            , TMifz(TMopr("<", TMvar("n")::TMint(0)::nil)
              , TMopr("+"
                , TMapp(TMvar("f")
                  , TMtup(
                      TMvar("sum")
                    , TMopr("-",TMvar("n")::TMint(1)::nil)
                    )
                  )
                ::TMapp(
                    TMvar("f")
                  , TMtup(
                      TMopr("-"
                      , TMvar("sum")
                      ::TMapp(TMvar("coin_get"), TMvar("n"))
                      ::nil
                      )
                      , TMvar("n")
                    )
                  ) 
                ::nil
                )
              , TMint(0)
              )
            )
          )
        )
      )
    , TMapp(TMvar("aux"), TMtup(TMvar("sum"), TMint(3)))
    )
  )
)



//val TMint1 = TMint(1)
//val () =
//println!("oftype(TMint1) = ", oftype0(TMint1))
//val () =
//println!("oftype(TMapp...) = ", oftype0(TMapp(TMint1, TMint1)))

val () = try
  println!("oftype(coin_change = ",
    eval(oftype0(TMapp(TM_coin_change, TMint(400))))
  )
  with ~IllTyped2(x,y) => println!("error:", x, " : ", y)

//val () =
//  println!("oftype(test = ", oftype0(TMlet("n",TMfst(TMtup(TMint(300), TMint(400))),TMvar("n"))))
val tuple_test =
  TMlam("x", none()
  , TMlet("y", TMtup(TMvar("x"), TMint(1))
    , TMlet("z"
      , TManno(TMfst(TMvar("y")), TPint)
      , TMvar("y")
      )
    )
  )

val () = println!("oftype tuple test = ", eval(oftype0(tuple_test)))
(* ****** ****** *)
// test case from Sid
val tpint_opt = some(TPint)
  val () = println!("\n~~Type check of prime check code from assign01 -- should return TMint(1) if prime, TMint(0) if not~~\n==Specific case here is checking if 13 is prime==")
  val check_13prime: expr = 
    TMifz(TMopr(">",TMint(13)::TMint(1)::nil), TMint(0),
        TMapp(TMfix("p", "n", tpint_opt, tpint_opt, TMifz(TMopr("<",TMvar("n")::TMint(13)::nil),TMint(1),
            TMifz(TMopr("%",TMint(13)::TMvar("n")::nil), TMint(0), 
              TMapp(TMvar("p"),
                TMopr("+",TMvar("n")::TMint(1)::nil))))), TMint(2)))
  val () = println!("Expression >>  {", check_13prime, "}")
  val () = println!("Return type is: ", eval(oftype0(check_13prime)))
  val () = println!()


val lxxx = TMlam("x", none, TMapp(TMvar("x"),TMvar("x")))

val () = println!("lam x. xx fails:", eval(oftype0(lxxx)))

implement main0() = ()

(* ****** ****** *)

(* end of [lambda3.dats] *)

