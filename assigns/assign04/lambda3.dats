(* ****** ****** *)

#include
"share\
/atspre_staload.hats"
#include
"share\
/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload
UN =
"prelude/SATS/unsafe.sats"

(* ****** ****** *)

#staload "./../../mylib/mylib.sats"
#staload "./../../mylib/mylib.dats"

(* ****** ****** *)
//
#define nil mylist_nil
//
#define :: mylist_cons
#define cons mylist_cons
//
(* ****** ****** *)
//
#define none myoptn_none
#define some myoptn_some
//
(* ****** ****** *)

abstype
tvar_type = ptr
typedef
tvar = tvar_type

(* ****** ****** *)

typedef tnam = string
typedef vnam = string
typedef opnm = string

(* ****** ****** *)
//
datatype type =
//
  | TPbas of tnam
//
  | TPext of tvar
//
  | TPfun of
    (type(*arg*), type(*res*))
  | TPtup of
    (type(*fst*), type(*snd*))
//
(* ****** ****** *)

typedef typeopt = myoptn(type)

(* ****** ****** *)
//
extern
fun
tvar_new(): tvar
and
type_new(): type
//
extern
fun
eq_tvar_tvar
(tvar, tvar): bool
overload = with eq_tvar_tvar
//
extern
fun
tvar_get_type
(tvar): myoptn(type)
extern
fun
tvar_set_type
(X: tvar, sol: type): void
//
overload .type with tvar_get_type
overload .type with tvar_set_type
//
(* ****** ****** *)

local

absimpl
tvar_type =
ref(typeopt)

in

implement
tvar_new() =
ref<typeopt>(none)

implement
eq_tvar_tvar
  (X, Y) =
(
  $UN.cast{ptr}(X)
  =
  $UN.cast{ptr}(Y)
)

implement
tvar_get_type(X) = X[]
implement
tvar_set_type(X, T) = (X[] := some(T))

end // end of [local]

(* ****** ****** *)

implement
type_new() =
TPext(tvar_new())

(* ****** ****** *)

fun
type_eval
(T: type): type =
(
case+ T of
| TPext(X) =>
  let
  val opt = X.type()
  in
  case+ opt of
  | none() => T
  | some(T) => type_eval(T)
  end
| _ (* non-TPext *) => T
)

overload eval with type_eval

(* ****** ****** *)

extern
fun
unify:
(type, type) -> bool

extern
fun
occurs(tvar, type): bool

implement
unify(T1, T2) =
let
val T1 = eval(T1)
val T2 = eval(T2)
//
fun
auxvar
(X1: tvar, T2: type): bool =
(
case+ T2 of
| TPext(X2) =>
  if X1 = X2
  then true
  else (X1.type(T2); true)
| _(*non-TPext*) =>
  if occurs(X1, T2)
  then false else (X1.type(T2); true)
)
//
in
case+
(T1, T2) of
|
(TPext(X1), _) => auxvar(X1, T2)
|
(_, TPext(X2)) => auxvar(X2, T1)
|
(TPbas(nm1), TPbas(nm2)) => nm1 = nm2
|
(TPfun(T11, T12), TPfun(T21, T22)) => 
 unify(T11, T21) && unify(T12, T22)
|
(TPtup(T11, T12), TPtup(T21, T22)) => 
 unify(T11, T21) && unify(T12, T22)
//
| (_, _) => false // unification failed
//
end

(* ****** ****** *)
(*
//
extern
fun
eq_type_type:
(type, type) -> bool
overload = with eq_type_type
//
*)
(* ****** ****** *)

extern
fun
print_type(type): void // stdout
and
prerr_type(type): void // stderr
and
fprint_type(FILEref, type): void

overload print with print_type
overload prerr with prerr_type
overload fprint with fprint_type

(* ****** ****** *)

typedef typeopt = myoptn(type)

(* ****** ****** *)

datatype expr = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of
    (vnam, typeopt, expr)
  | TMapp of (expr, expr)
//
  | TMifz of (expr, expr, expr)
//
  | TMfix of
    ( vnam, vnam
    , typeopt(*arg*)
    , typeopt(*res*), expr) // Y(lam f.lam x.<body>)
//
  | TMopr of (opnm, exprlst)
//
  | TMtup of (expr, expr)
  | TMfst of expr | TMsnd of expr
//
  | TMlet of (vnam, expr, expr) // let x = t1 in t2 end
//
  | TManno of (expr, type)
//
where exprlst = mylist(expr)

(* ****** ****** *)

extern
fun
print_expr(expr): void // stdout
and
prerr_expr(expr): void // stderr
and
fprint_expr(FILEref, expr): void

overload print with print_expr
overload prerr with prerr_expr
overload fprint with fprint_expr

(* ****** ****** *)

(*
implement
eq_type_type =
lam(tp1, tp2) =>
(
case+
(tp1, tp2) of
| (TPbas nm1,
   TPbas nm2) => (nm1 = nm2)
| (TPfun(tp11, tp12), 
   TPfun(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (TPtup(tp11, tp12), 
   TPtup(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (_, _) => false
)
*)

(* ****** ****** *)

implement
print_type(tp) =
fprint_type(stdout_ref, tp)
implement
prerr_type(tp) =
fprint_type(stderr_ref, tp)

(* ****** ****** *)

implement
fprint_type(out, tp0) =
(
case+ tp0 of
| TPext(tv) =>
  fprint!(out, "TPvar(...)")
| TPbas(nm) =>
  fprint!(out, "TPbas(", nm, ")")
| TPfun(tp1, tp2) =>
  fprint!(out, "TPfun(", tp1, ", ", tp2, ")")
| TPtup(tp1, tp2) =>
  fprint!(out, "TPtup(", tp1, ", ", tp2, ")")
)

(* ****** ****** *)

implement
print_expr(tm) =
fprint_expr(stdout_ref, tm)
implement
prerr_expr(tm) =
fprint_expr(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<type> = fprint_type
implement
fprint_val<expr> = fprint_expr

(* ****** ****** *)

implement
fprint_expr(out, tm0) =
(
case+ tm0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMstr(x) =>
  fprint!(out, "TMstr(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tp0, tm1) =>
  fprint!
  ( out
  , "TMlam(", x, "; ", tp0, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, x, tp1, tp2, tm1) =>
  fprint!(out, "TMfix(", f, "; ", x, "; ", tp1, "; ", tp2, tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
//
| TMfst(tm1) =>
  fprint!(out, "TMfst(", tm1, ")")
| TMsnd(tm1) =>
  fprint!(out, "TMsnd(", tm1, ")")
| TMtup(tm1, tm2) =>
  fprint!(out, "TMtup(", tm1, "; ", tm2, ")")
//
| TMlet(x0, tm1, tm2) =>
  fprint!(out, "TMlet(", x0, "; ", tm1, "; ", tm2, ")")
//
| TManno(tm1, tp2) =>
  fprint!(out, "TManno(", tm1, "; ", tp2, ")")
)

(* ****** ****** *)

exception IllTyped0 of ()
exception IllTyped1 of (expr)
exception IllTyped2 of (expr, type)

(* ****** ****** *)

typedef tctx =
list0($tup(vnam, type))

(* ****** ****** *)

extern
fun oftype0(expr): type
extern
fun oftype1(tctx, expr): type

(* ****** ****** *)

val TPint = TPbas("int")
val TPstr = TPbas("string")
val TPbool = TPbas("bool")
val TPvoid = TPbas("void")

(* ****** ****** *)

implement
oftype0(tm0) =
oftype1(list0_nil(), tm0)

implement
oftype1(tctx0, tm0) =
(
case+ tm0 of
//
| TMint _ => TPint
| TMstr _ => TPstr
//
(*
  | TMvar of vnam
*)
//
  | TMlam
    (x0, tp1, tm2) =>
    let
      val tp1 =
      (
      case+ tp1 of
      | none() =>
        type_new()
      | some(tp1) => tp1
      ) : type // end-of-val
      val
      tctx1 =
      list0_cons
      ($tup(x0, tp1), tctx0)
    in
      TPfun(tp1, oftype1(tctx1, tm2))
    end
//
  | TMapp(tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    val tp1 =
    (
    case+ eval(tp1) of
    | TPext(X) =>
      let
      val T = TPfun(T1, T2) in (X.type(T); T)
      end where
      {
        val T1 = type_new()
        val T2 = type_new()
      }
    | _ => tp1
    ) : type
    in
      case- tp1 of
      | TPfun(tp11, tp12) =>
        if
        unify(tp11, tp2)
        then tp12 else $raise IllTyped0()
    end
//
  | TMifz(tm1, tm2, tm3) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    val tp3 = oftype1(tctx0, tm3)
    in
      if
      unify(tp1, TPint)
      then
      (
      if unify(tp2, tp3) then tp2 else $raise IllTyped0()
      )
      else $raise IllTyped0()
    end
//
(*
  | TMfix _ => ...
  | TMopr _ => ...
  | TMtup _ => ...
  | TMfst _ => ...
  | TMsnd _ => ...
  | TMlet _ => ...
  | TManno _ => ...
*)
)

(* ****** ****** *)

(*
val TMint1 = TMint(1)
val () =
println!("oftype(TMint1) = ", oftype0(TMint1))
val () =
println!("oftype(TMapp...) = ", oftype0(TMapp(TMint1, TMint1)))
*)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [lambda3.dats] *)

